-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2020 at 04:59 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `karyawan`
--

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(7);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan_master`
--

CREATE TABLE `karyawan_master` (
  `id` int(11) NOT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `goldar` varchar(255) DEFAULT NULL,
  `jeniskelamin` varchar(255) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `kelurahan` varchar(255) DEFAULT NULL,
  `kewarganegaraan` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `pekerjaan` varchar(255) DEFAULT NULL,
  `rtrw` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tanggallahir` varchar(255) DEFAULT NULL,
  `tempatlahir` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karyawan_master`
--

INSERT INTO `karyawan_master` (`id`, `agama`, `alamat`, `goldar`, `jeniskelamin`, `kecamatan`, `kelurahan`, `kewarganegaraan`, `nama`, `nik`, `pekerjaan`, `rtrw`, `status`, `tanggallahir`, `tempatlahir`) VALUES
(4, 'Islam', 'Perum. Bumi Damai', 'A', 'Laki-laki', 'Gembor', 'Gembor', 'WNI', 'Siapa', '3671085107970004', 'Karyawan', '01/02', NULL, '1979-10-15', 'Jakarta'),
(3, 'Islam', 'Perum. Bumi Indah', 'O', 'Laki-laki', 'Periuk', 'Gembor', 'WNI', 'Louis', '3671654892635002', 'Karyawan', '02/02', 'Menikah', '', 'Jakarta'),
(6, 'Islam', 'Perum. Bumi Damai', 'A', 'Laki-laki', 'Gembor', 'Gembor', 'WNI', 'Sudrajat', '3671085107970004', 'Karyawan', '01/02', 'Belum Kawin', '1979-10-15', 'Jakarta');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `karyawan_master`
--
ALTER TABLE `karyawan_master`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
