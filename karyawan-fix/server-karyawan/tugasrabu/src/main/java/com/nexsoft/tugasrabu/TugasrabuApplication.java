package com.nexsoft.tugasrabu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TugasrabuApplication {

	public static void main(String[] args) {
		SpringApplication.run(TugasrabuApplication.class, args);
	}

}
