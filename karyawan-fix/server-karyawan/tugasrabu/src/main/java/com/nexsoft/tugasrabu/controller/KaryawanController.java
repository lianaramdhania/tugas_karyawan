package com.nexsoft.tugasrabu.controller;

import com.nexsoft.tugasrabu.entity.Karyawan;
import com.nexsoft.tugasrabu.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class KaryawanController {
    @Autowired
    private KaryawanService service;

    @PostMapping("/addKaryawan")
    public Karyawan addKaryawan(@RequestBody Karyawan karyawan) {
        return service.saveKaryawan(karyawan);
    }

    @PostMapping("/addKaryawans")
    public List<Karyawan> addKaryawans(@RequestBody List<Karyawan> karyawans) {
        return service.saveKaryawans(karyawans);
    }

    @GetMapping("/karyawans")
    public List<Karyawan> findAllKaryawans(){
        return service.getKaryawans();
    }

    @GetMapping("/karyawanById/{id}")
    public Karyawan findKaryawanById(@PathVariable int id) {
        return service.getKaryawanById(id);
    }

    @GetMapping("/karyawanByNama{nama}")
    public Karyawan findKaryawanByNama(@PathVariable String nama) {
        return service.getKaryawanByNama(nama);
    }

    //Put itu buat update
    @PutMapping("/update/{id}")
    public void updateKaryawan(@RequestBody Karyawan karyawan, @PathVariable int id) {
        karyawan.setId(id);
        service.saveKaryawan(karyawan);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteKaryawan(@PathVariable int id) {
        return service.deleteKaryawan(id);
    }


}
