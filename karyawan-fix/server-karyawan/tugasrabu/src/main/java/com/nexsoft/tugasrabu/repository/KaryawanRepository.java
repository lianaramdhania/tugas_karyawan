package com.nexsoft.tugasrabu.repository;

import com.nexsoft.tugasrabu.entity.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KaryawanRepository extends JpaRepository<Karyawan, Integer> {
    Karyawan findByNama(String nama);
}