package com.nexsoft.tugasrabu.service;

import com.nexsoft.tugasrabu.entity.Karyawan;
import com.nexsoft.tugasrabu.repository.KaryawanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KaryawanService {
    @Autowired                      //otomatis tipe datanya dari mana
    private KaryawanRepository repository;

    public Karyawan saveKaryawan(Karyawan karyawan) {
        return repository.save(karyawan);
    }

    //save sekaligus banyak
    public List<Karyawan> saveKaryawans(List<Karyawan> karyawans) {
        return repository.saveAll(karyawans);
    }

    public List<Karyawan> getKaryawans() {
        return repository.findAll();
    }

    public Karyawan getKaryawanById(int id) {
        return repository.findById(id).orElse(null);
    }

    public Karyawan getKaryawanByNama(String nama) {
        return repository.findByNama(nama);
    }

    public String deleteKaryawan(int id) {
        repository.deleteById(id);
        return "Karyawan removed!!";
    }

    public Karyawan updateKaryawan(Karyawan karyawan) {
        Karyawan existingKaryawan = repository.findById(karyawan.getId()).orElse(null);
        existingKaryawan.setNik(karyawan.getNik());
        existingKaryawan.setNama(karyawan.getNama());
        existingKaryawan.setAlamat(karyawan.getAlamat());
        return repository.save(existingKaryawan);
    }
}