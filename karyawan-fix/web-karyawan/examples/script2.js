const url = "http://localhost:8080/"
var detail = 'http://127.0.0.1:5500/web-karyawan/examples/detaildata.html?id'

//List Data
function getData(url) {
    let xmlhttp = new XMLHttpRequest()
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let resp = JSON.parse(this.responseText)            //JSON.parse mengubah dari string ke json
            let msgReceipt = " "
            for (let i = 0; i < resp.length; i++) {
                msgReceipt += `
                <tr><td><a href="detaildata.html?id=`+resp[i].id+`">`+resp[i].nik+`<td>`+resp[i].nama+`<td> `+  resp[i].jeniskelamin +`  </a></td></tr>
                `
            }
            document.getElementById("nama").innerHTML = msgReceipt
        }
    }
    xmlhttp.open("GET", url, true)
    xmlhttp.send()
}

//Detail
function getItemById(id) {
    fetch(`http://localhost:8080/karyawanById/`+id)
        .then(response => response.json())
        .then(data => renderItemsById(data))
        .catch(console.error);
            
}

const renderItemsById = (karyawans) =>{
    let karyawan = `
        <tr><td> NIK </td><td>:</td> <td> `+ karyawans.nik+`</td></tr><tr><td> Nama </td><td>:</td> <td>`+karyawans.nama+` 
        </td></tr><tr><td> Tempat,tanggal lahir </td><td>:</td> <td>`+karyawans.tempatlahir+`,`+karyawans.tanggallahir+`
        </td></tr><tr><td> Jenis Kelamin </td><td>:</td> <td>`+karyawans.jeniskelamin+`
        </td></tr><tr><td> Golongan Darah </td><td>:</td> <td>`+karyawans.goldar+`
        </td></tr><tr><td> Alamat </td><td>:</td> <td>`+karyawans.alamat +` Rt/Rw `+karyawans.rtrw+` `+karyawans.kelurahan+`
        </td></tr><tr><td> Kecamatan </td><td>:</td> <td>`+karyawans.kecamatan+`
        </td></tr><tr><td> Agama </td><td>:</td> <td>`+karyawans.agama+`
        </td></tr><tr><td> Status Perkawinan </td><td>:</td> <td>`+karyawans.status+`
        </td></tr><tr><td> Pekerjaan </td><td>:</td> <td>`+karyawans.pekerjaan+`
        </td></tr><tr><td> Kewarganegaraan </td><td>:</td> <td>`+karyawans.kewarganegaraan+`
        </td></tr> <br> <br>
        <button onclick="deletedata(`+karyawans.id+`)" type="submit" class="btn btn-danger btn-fill pull-right id="hapus"  style="margin-left: 30px;">Hapus</button>
        <a href="update.html?id=`+karyawans.id+`"><input type="button" class="btn btn-warning btn-fill" id="Edit" value="Edit"></a>
    `
    document.getElementById("detaildata").innerHTML = karyawan

}

function deletedata (id) {
    console.log();
    var strconfirm = confirm("Apakah Benar Ingin Dihapus?");
    if (strconfirm == true) {
        fetch(`${url}/delete/${id}`, {
            method: "DELETE",
        })
        .then(res => location.reload())
    }
    
};

