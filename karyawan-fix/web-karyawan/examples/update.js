const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const idParams = urlParams.get('id')
console.log(idParams);

function getItemById() {
    fetch(`http://localhost:8080/karyawanById/` + idParams)
        .then(response => response.json())
        .then(data => renderItemsById(data))
        .catch(console.error);
}
const renderItemsById = (data) => {
    let myResp = `
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Menambahkan Daftar</h4>
                <p class="card-category">Silahkan Edit Data yang Sesuai!</p>
              </div>
              <div class="card-body">
                <form  method="post">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="bmd-label-floating">NIK</label>
                        <input type="text" id="nik" value="${data.nik}" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label class="bmd-label-floating">Nama</label>
                        <input type="text" id="nama" value="${data.nama}" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="bmd-label-floating">Tempat Lahir</label>
                        <input type="text" id="tempatlahir" value="${data.tempatlahir}" class="form-control">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="bmd-label-floating">Tanggal Lahir</label>
                        <input type="date" id="tanggallahir" value="${data.tanggallahir}" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="bmd-label-floating">Jenis Kelamin</label>
                        <input type="text" id="jeniskelamin" value="${data.jeniskelamin}" class="form-control">
                      </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Agama</label>
                          <input type="text" id="agama" value="${data.agama}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="bmd-label-floating">Status Perkawinan</label>
                        <input type="text" id="status" value="${data.status}" class="form-control">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="bmd-label-floating">Alamat</label>
                        <input type="text" id="alamat" value="${data.alamat}" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="bmd-label-floating">RT/RW</label>
                        <input type="text" id="rtrw" value="${data.rtrw}" class="form-control">
                      </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="bmd-label-floating">Kelurahan</label>
                      <input type="text" id="kelurahan" value="${data.kelurahan}" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="bmd-label-floating">Kecamatan</label>
                    <input type="text" id="kecamatan" value="${data.kecamatan}" class="form-control">
                  </div>
              </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="bmd-label-floating">Pekerjaan</label>
                        <input type="text" id="pekerjaan" value="${data.pekerjaan}" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="bmd-label-floating">Kewarganegaraan</label>
                        <input type="text" id="kewarganegaraan" value="${data.kewarganegaraan}" class="form-control">
                      </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="bmd-label-floating">Golongan Darah</label>
                      <input type="text" id="goldar" value="${data.goldar}" class="form-control">
                    </div>
                </div>
                  </div>
                </div>
                  
                  <!-- <button type="submit" id ="submit" method="post" class="btn btn-primary pull-right">Update Profile</button> -->
                  <button class="btn btn-info btn-fill pull-right"" onclick="update()" id="kirim" type="submit" name="action">Update Data</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
  
    
    `
    document.getElementById("updatekaryawan").innerHTML = myResp
}
function update(id) {
  var nik = document.getElementById("nik").value;
  var nama = document.getElementById("nama").value;
  var tempatlahir = document.getElementById("tempatlahir").value;
  var tanggallahir = document.getElementById("tanggallahir").value;
  var jeniskelamin = document.getElementById("jeniskelamin").value;
  var alamat = document.getElementById("alamat").value;
  var rtrw = document.getElementById("rtrw").value;
  var kelurahan = document.getElementById("kelurahan").value;
  var kecamatan = document.getElementById("kecamatan").value;
  var agama = document.getElementById("agama").value;
  var status = document.getElementById("status").value;
  var pekerjaan = document.getElementById("pekerjaan").value;
  var kewarganegaraan = document.getElementById("kewarganegaraan").value;
  var goldar = document.getElementById("goldar").value;

    var str_json = JSON.stringify({
      nik:  nik,
      nama: nama,
      tempatlahir: tempatlahir,
      tanggallahir: tanggallahir,
      jeniskelamin: jeniskelamin,
      alamat: alamat,
      rtrw: rtrw,
      kelurahan: kelurahan,
      kecamatan: kecamatan,
      agama: agama,
      status: status,
      pekerjaan: pekerjaan,
      kewarganegaraan: kewarganegaraan,
      goldar: goldar
  })
  console.log(str_json)
  fetch('http://localhost:8080/update/' + idParams, {
          method: 'PUT',
          headers: {
              'Content-Type': 'application/json',
          },
          body: str_json,
      })
      // .then(response => response.json())
      .then(data => {
          alert("berhasil")
          console.log('Success:', data);
          window.location.href = "update.html"
      })
      .catch((error) => {
          console.error('Error:', error);
      });
      window.location.href = "update.html"

    }

    getItemById()